package main

import (
	"bufio"
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/eiannone/keyboard"
	_ "github.com/nakagami/firebirdsql"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/net/html/charset"
	"golang.org/x/text/encoding/charmap"
)

type ProdutosServicoEmpresa struct {
	ProdutoServico []CadastroDescricao
	Preco          []Preco
	Participacao   string
}

type CadastroDescricao struct {
	CodigoInterno  string
	Descricao      string
	CodigoBarras   string
	Caracteristica string
	Pesavel        bool
	Ativo          bool
	UnidadeMedida  unidadeMed
}

type unidadeMed struct {
	Descricao string
	Sigla     string
}

type Preco struct {
	Venda VendaPreco
	Custo PrecoCusto
}

type VendaPreco struct {
	Valor float64
	Hash  string
}

type PrecoCusto struct {
	Valor float64
}

type DatabaseDatas struct {
	FirebirdUser     string `json:"firebirdUser"`
	FirebirdPassword string `json:"firebirdPassword"`
	FirebirdDatabase string `json:"firebirdDatabase"`
	FirebirdIP       string `json:"firebirdIP"`
	FirebirdPort     string `json:"firebirdPort"`
	MongoUser        string `json:"mongoUser"`
	MongoPassword    string `json:"mongoPassword"`
	MongoDatabase    string `json:"mongoDatabase"`
	MongoIP          string `json:"mongoIP"`
	MongoPort        string `json:"mongoPort"`
}

type countdown struct {
	t int
	d int
	h int
	m int
	s int
}

var clientMongo *mongo.Client
var collectionMongo *mongo.Collection
var err error
var ctxMongo context.Context
var wg sync.WaitGroup
var connDB *sql.DB
var userDB, passwordDB, databaseDB, ipDB, portDB string

func handleErrors(errorVar error) {
	if errorVar != nil {
		log.Fatal(errorVar.Error())
	}
}

/*func insertOneDocument() {
	var insertResult *mongo.InsertOneResult

	ruan := Person{primitive.ObjectID(), "Ruan", 34, "Cape Town"}

	insertResult, err = collectionMongo.InsertOne(ctxMongo, ruan)

	handleErrors(err)

	fmt.Println("Inserted a Single Document: ", insertResult.InsertedID)
}

func insertManyDocument() {
	var insertManyResult *mongo.InsertManyResult

	antonio := Person{"2", "Antonio", 34, "Cape Town"}
	james := Person{"3", "James", 32, "Nairobi"}
	frankie := Person{"4", "Frankie", 31, "Nairobi"}

	trainers := []interface{}{antonio, james, frankie}

	insertManyResult, err = collectionMongo.InsertMany(ctxMongo, trainers)

	handleErrors(err)

	fmt.Println("Inserted multiple documents: ", insertManyResult.InsertedIDs)
}

func updateDocument() {
	var updateResult *mongo.UpdateResult

	filter := bson.D{
		{"age", 31},
	}
	update := bson.D{
		{"$inc", bson.D{
			{"age", 1},
		}},
	}

	updateResult, err = collectionMongo.UpdateOne(ctxMongo, filter, update)

	handleErrors(err)

	fmt.Printf("Matched %v documents and updated %v documents.\n", updateResult.MatchedCount, updateResult.ModifiedCount)
}

func deleteDocument() {
	var deleteResult *mongo.DeleteResult

	filter := bson.D{
		{"age", 32},
	}
	deleteResult, err = collectionMongo.DeleteMany(ctxMongo, filter)

	handleErrors(err)

	fmt.Printf("Deleted %v documents in the trainers collection\n", deleteResult.DeletedCount)
}

func findOneDocument() {
	filter := bson.D{
		{"age", 34},
	}
	var result Person

	err = collectionMongo.FindOne(ctxMongo, filter).Decode(&result)

	handleErrors(err)

	fmt.Printf("Found a single document: %+v\n", result)

	findOptions := options.Find()
	findOptions.SetLimit(2)
}*/
/*
revenda
r3v3nd@
12220
*/

func connectMongo() *mongo.Client {
	ctxMongo = context.TODO()
	// clientOptions := options.Client().ApplyURI("mongodb://" + ipDB + ":" + portDB)
	clientOptions := options.Client().ApplyURI("mongodb://" + userDB + ":" + url.QueryEscape(passwordDB) + "@" + ipDB + ":" + portDB + "/?authSource=" + databaseDB)
	clientMongo, err = mongo.Connect(ctxMongo, clientOptions)

	handleErrors(err)

	err = clientMongo.Ping(ctxMongo, nil)

	handleErrors(err)

	fmt.Println("Connected to MongoDB!")

	return clientMongo
}

func disconnectMongo() {
	err = clientMongo.Disconnect(ctxMongo)

	handleErrors(err)
}

func connectFirebird() {
	// var driver, user, database, password, server, port, collaction string
	var collaction, driver string

	driver = "firebirdsql"
	/*user = "SYSDBA"
	password = "123456"
	// password = "masterkey"
	database = "/var/www/html/Firebird/migracao.fdb"
	// database = "C:\\RepleisConta\\Dados\\DADOS.FDB"
	// server = "127.0.0.1"
	server = "localhost"
	port = "3050"*/
	collaction = "?COLLATE=WIN_PTBR&CHARACTER=WIN1252"

	connDB, err = sql.Open(driver, userDB+":"+passwordDB+"@"+ipDB+":"+portDB+"/"+databaseDB+collaction)
	handleErrors(err)

	err := connDB.Ping()
	handleErrors(err)
}

func closeDBFirebird() {
	connDB.Close()
}

func findManyDocumets() []ProdutosServicoEmpresa {
	fmt.Println("Fetching datas in the MongoDB!")

	var results []ProdutosServicoEmpresa
	var cur *mongo.Cursor

	/*findOptions := options.Find()
	findOptions.SetProjection(bson.D{
		{"ProdutoServico", 1},
		{"Preco", 1},
	})

	findOptions.SetLimit(100)

	filter := bson.M{}

	cur, err = collectionMongo.Find(ctxMongo, filter, findOptions)*/

	lookupProduto := bson.D{{"$lookup", bson.D{{"from", "ProdutosServicos"}, {"localField", "ProdutoServicoReferencia"}, {"foreignField", "_id"}, {"as", "ProdutoServico"}}}}
	lookupPreco := bson.D{{"$lookup", bson.D{{"from", "Precos"}, {"localField", "PrecoReferencia"}, {"foreignField", "_id"}, {"as", "Preco"}}}}
	project := bson.D{{"$project", bson.D{{"ProdutoServico", 1}, {"Preco", 1}, {"Participacao", "Disponível"}}}}
	// limit := bson.D{{"$limit", 150}}

	// unwindStage := bson.D{{"$unwind", bson.D{{"path", "$Preco"}, {"preserveNullAndEmptyArrays", false}}}}
	cur, err := collectionMongo.Aggregate(ctxMongo, mongo.Pipeline{lookupProduto, lookupPreco, project})

	handleErrors(err)

	for cur.Next(ctxMongo) {
		var elem ProdutosServicoEmpresa
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}

		results = append(results, elem)
	}

	err = cur.Err()

	handleErrors(err)

	cur.Close(ctxMongo)

	return results
}

func listRegisterFirebird() {
	selDB, err := connDB.Query("SELECT * FROM CadastroDescricao")
	handleErrors(err)

	for selDB.Next() {
		var cadDesc CadastroDescricao

		err = selDB.Scan(&cadDesc.CodigoInterno, &cadDesc.Descricao, &cadDesc.CodigoBarras,
			&cadDesc.UnidadeMedida)
		handleErrors(err)

		fmt.Println(cadDesc)
	}
}

func getRegisterByIdFirebird(id string) (bool, int) {
	var codigo int

	codInt := convertCodigoInterno(id)

	row := connDB.QueryRow("SELECT Codigo FROM CadastroDescricao where CodigoInterno=?", codInt)
	err = row.Scan(&codigo)

	if err != nil {
		//nao retornou nada
		return false, codigo
	}

	return true, codigo
}
func getMaxCodeFirebird() int {
	var codigo int

	row := connDB.QueryRow("SELECT max(Codigo) FROM CadastroDescricao")
	err = row.Scan(&codigo)

	return codigo
}

func updateDataFirebird(value ProdutosServicoEmpresa) {
	if value.ProdutoServico[0].Ativo {
		updateData, err := connDB.Prepare(`update CadastroDescricao set Descricao=?,
			CodigoBarras=?, UN=?, Preco=?, Estoque=?,PrecoCompra=? where CodigoInterno=?`)
		handleErrors(err)

		codInt := convertCodigoInterno(value.ProdutoServico[0].CodigoInterno)

		if value.ProdutoServico[0].Pesavel && len(value.ProdutoServico[0].Caracteristica) > 0 {
			value.ProdutoServico[0].CodigoBarras = value.ProdutoServico[0].Caracteristica
		} else {
			if len(value.ProdutoServico[0].CodigoBarras) > 13 {
				value.ProdutoServico[0].CodigoBarras = ""
			}
		}

		updateData.Exec(value.ProdutoServico[0].Descricao, value.ProdutoServico[0].CodigoBarras,
			value.ProdutoServico[0].UnidadeMedida.Sigla, value.Preco[0].Venda.Valor, 0, value.Preco[0].Custo.Valor, codInt)

		handleErrors(err)
	}
}

func convertCodigoInterno(codigoInterno string) int {
	codigoInterno = strings.Replace(codigoInterno, "X", "22", 1)
	re := regexp.MustCompile("[0-9]+")
	filterString := re.FindAllString(codigoInterno, -1)[0]

	codInt, _ := strconv.Atoi(filterString)

	return codInt
}

func updateDisponivel(value ProdutosServicoEmpresa) {
	updateData, err := connDB.Prepare(`update CadastroDescricao set Participacao=Descricao, 
		Descricao=? where CodigoInterno=?`)
	handleErrors(err)

	codInt := convertCodigoInterno(value.ProdutoServico[0].CodigoInterno)

	updateData.Exec(value.ProdutoServico[0].Descricao, codInt)

	handleErrors(err)
}

func convrtToUTF8(str string, origEncoding string) string {
	strBytes := []byte(str)
	byteReader := bytes.NewReader(strBytes)
	reader, _ := charset.NewReaderLabel(origEncoding, byteReader)
	strBytes, _ = ioutil.ReadAll(reader)
	return string(strBytes)
}

func convertToWindows1252(str string) string {
	sr := strings.NewReader(str)
	tr := charmap.Windows1252.NewDecoder().Reader(sr)

	b, e := ioutil.ReadAll(tr)
	if e != nil {
		fmt.Println("error:", e)
	}

	return string(b)
}

func insertRegisterFirebird(value ProdutosServicoEmpresa, codigoCont int) {
	if value.ProdutoServico[0].Ativo {
		insForm, err := connDB.Prepare(`INSERT INTO CadastroDescricao(CODIGO, CodigoInterno, Descricao,
			CodigoBarras, UN, Preco, Estoque, PrecoCompra) VALUES(?,?,?,?,?,?,?,?)`)
		handleErrors(err)

		codInt := convertCodigoInterno(value.ProdutoServico[0].CodigoInterno)
		if value.ProdutoServico[0].Pesavel && len(value.ProdutoServico[0].Caracteristica) > 0 {
			value.ProdutoServico[0].CodigoBarras = value.ProdutoServico[0].Caracteristica
		} else {
			if len(value.ProdutoServico[0].CodigoBarras) > 13 {
				value.ProdutoServico[0].CodigoBarras = ""
			}
		}

		_, err = insForm.Exec(codigoCont, codInt, value.Participacao, value.ProdutoServico[0].CodigoBarras, value.ProdutoServico[0].UnidadeMedida.Sigla, value.Preco[0].Venda.Valor, 0, value.Preco[0].Custo.Valor)
		handleErrors(err)
	}
}

func typeTryData(mongoRegs []ProdutosServicoEmpresa, done chan<- bool) {
	// Prepara a SQL e verifica errors
	var ret bool
	var codigoTable int
	var beforeCode = 0
	var value ProdutosServicoEmpresa

	for _, value = range mongoRegs {
		connectFirebird()
		ret, codigoTable = getRegisterByIdFirebird(value.ProdutoServico[0].CodigoInterno)

		if ret {
			updateDataFirebird(value)
			beforeCode = codigoTable
		} else {
			beforeCode = getMaxCodeFirebird()
			beforeCode++
			insertRegisterFirebird(value, beforeCode)

			updateDisponivel(value)
		}

		closeDBFirebird()
	}

	done <- true
}

func loader(done <-chan bool) {
	defer wg.Done()
	fmt.Println("Inserting datas in Firebird!")
	i := 0
	load := []rune(`|/-\|/-\`)

	for {
		select {
		case <-done:
			fmt.Println("\nDone")
			return

		default:
			fmt.Printf("\r")
			fmt.Print(string(load[i]))
			i++

			if i == len(load) {
				i = 0
			}
		}
	}
}

func cleanFirebird() {
	fmt.Println("Cleanning the Firebird")
	connectFirebird()
	// Prepara a SQL e verifica errors
	delForm, err := connDB.Prepare("DELETE FROM CadastroDescricao")
	handleErrors(err)

	delForm.Exec()
	closeDBFirebird()
}

func menu() int {
	fmt.Println("\n\n====Database replication====")
	fmt.Println("1.Informar os dados\n2.Ver a lista anterior\n3.Fazer a replicação\n4.Sair\n")
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Opção: ")
	scanner.Scan()
	opcDig := scanner.Text()

	opc, _ := strconv.Atoi(opcDig)
	return opc
}

func putDatas() []DatabaseDatas {
	var databaseDatas []DatabaseDatas
	var qtde int

	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Quantidade de IPs a informar: ")
	scanner.Scan()
	qtdeStr := scanner.Text()
	qtde, _ = strconv.Atoi(qtdeStr)

	fmt.Print("A lista anterior será mantida? (s/n) ")
	scanner.Scan()
	manter := scanner.Text()

	if manter == "s" {
		databaseDatas = readFileDatas()
	}

	cont := 0
	for cont < qtde {
		var newDatabase DatabaseDatas

		fmt.Printf("Dados do MongoDB (Banco %d)\n", cont+1)

		fmt.Print("Usuario: ")
		scanner.Scan()
		newDatabase.MongoUser = scanner.Text()

		fmt.Print("Senha: ")
		scanner.Scan()
		newDatabase.MongoPassword = scanner.Text()

		fmt.Print("Nome do Banco(ex: DigisatServer): ")
		scanner.Scan()
		newDatabase.MongoDatabase = scanner.Text()

		fmt.Print("IP (se estiver na propria maquina, digite 127.0.0.1): ")
		scanner.Scan()
		newDatabase.MongoIP = scanner.Text()

		fmt.Print("Porta (normalmente, é 27017): ")
		scanner.Scan()
		newDatabase.MongoPort = scanner.Text()

		fmt.Printf("Dados do Firebird (Banco %d)\n", cont+1)

		fmt.Print("Usuario: ")
		scanner.Scan()
		newDatabase.FirebirdUser = scanner.Text()

		fmt.Print("Senha: ")
		scanner.Scan()
		newDatabase.FirebirdPassword = scanner.Text()

		fmt.Print("Caminho do banco (ex: C:\\RepleisConta\\Dados\\DADOS.FDB): ")
		scanner.Scan()
		newDatabase.FirebirdDatabase = scanner.Text()

		fmt.Print("IP (se estiver na propria maquina, digite 127.0.0.1): ")
		scanner.Scan()
		newDatabase.FirebirdIP = scanner.Text()

		fmt.Print("Porta (normalmente, é 3050): ")
		scanner.Scan()
		newDatabase.FirebirdPort = scanner.Text()
		databaseDatas = append(databaseDatas, newDatabase)

		cont++
	}
	return databaseDatas
}

func readFileDatas() []DatabaseDatas {
	var databaseDatas []DatabaseDatas
	file, _ := ioutil.ReadFile("replicationDatas.json")

	_ = json.Unmarshal([]byte(file), &databaseDatas)
	return databaseDatas
}

func getTimeRemaining(t time.Time) countdown {
	currentTime := time.Now()
	difference := t.Sub(currentTime)

	total := int(difference.Seconds())
	days := int(total / (60 * 60 * 24))
	hours := int(total / (60 * 60) % 24)
	minutes := int(total/60) % 60
	seconds := int(total % 60)

	return countdown{
		t: total,
		d: days,
		h: hours,
		m: minutes,
		s: seconds,
	}
}

func main() {
	var mongoRegister []ProdutosServicoEmpresa
	var opc int
	var databaseDatas []DatabaseDatas
	var event keyboard.KeyEvent

	for true {
		opc = 0
		keysEvents, err := keyboard.GetKeys(10)
		if err != nil {
			panic(err)
		}
		go func() {
			fmt.Println("\nPress ESC to cancel\n")

			event = <-keysEvents
			if event.Err != nil {
				panic(event.Err)
			}
		}()

		timeLater := time.Now().Add(time.Second * time.Duration(10))
		for range time.Tick(1 * time.Second) {
			timeRemaining := getTimeRemaining(timeLater)

			fmt.Printf("\r")
			fmt.Print("Starting replication in: ", timeRemaining.s, "s")

			if timeRemaining.t <= 0 {
				_ = keyboard.Close()
				opc = 3
				break
			}
			if event.Key == keyboard.KeyEsc {
				_ = keyboard.Close()
				opc = menu()
				break
			}
		}

		switch opc {
		case 1:
			databaseDatas = putDatas()
			file, _ := json.MarshalIndent(databaseDatas, "", " ")
			_ = ioutil.WriteFile("replicationDatas.json", file, 0644)
		case 2:
			databaseDatas = readFileDatas()
			for k, v := range databaseDatas {
				fmt.Printf("Dados do MongoDB (Banco %d)\n", k+1)

				fmt.Printf("Usuario: %s\n", v.MongoUser)
				fmt.Printf("Senha: %s\n", v.MongoPassword)
				fmt.Printf("Banco: %s\n", v.MongoDatabase)
				fmt.Printf("IP: %s\n", v.MongoIP)
				fmt.Printf("Porta: %s\n\n", v.MongoPort)

				fmt.Printf("Dados do Firebird (Banco %d)\n", k+1)

				fmt.Printf("Usuario: %s\n", v.FirebirdUser)
				fmt.Printf("Senha: %s\n", v.FirebirdPassword)
				fmt.Printf("Banco: %s\n", v.FirebirdDatabase)
				fmt.Printf("IP: %s\n", v.FirebirdIP)
				fmt.Printf("Porta: %s\n\n", v.FirebirdPort)
			}
		case 3:
			databaseDatas = readFileDatas()

			for k, v := range databaseDatas {
				fmt.Printf("\n\nFetching MongoDB datas (Banco %d)\n", k+1)

				userDB = v.MongoUser
				passwordDB = v.MongoPassword
				databaseDB = v.MongoDatabase
				ipDB = v.MongoIP
				portDB = v.MongoPort

				clientMongo = connectMongo()
				collectionMongo = clientMongo.Database(databaseDB).Collection("ProdutosServicosEmpresa")

				mongoRegister = findManyDocumets()
				fmt.Println("", len(mongoRegister), "registers recovered of MongoDB")

				disconnectMongo()

				userDB = v.FirebirdUser
				passwordDB = v.FirebirdPassword
				databaseDB = v.FirebirdDatabase
				ipDB = v.FirebirdIP
				portDB = v.FirebirdPort

				done := make(chan bool)

				go typeTryData(mongoRegister, done)
				wg.Add(1)
				go loader(done)

				wg.Wait()
			}
		case 4:
			os.Exit(0)
		default:
			fmt.Println("Opção inválida!")
		}
	}
}

// GOOS=windows GOARCH=amd64 go build -o handell-sistemas.exe main.go

// connect "/var/www/html/Firebird/migracao.fdb" user 'SYSDBA' password '123456';

// db.ProdutosServicos.aggregate([ { $lookup: { from: "Precos", localField: "Hash", foreignField: "Venda.Hash", as: "PrecosVendas" } } ]).pretty()
// db.ProdutosServicosEmpresa.aggregate([ { $lookup: { from: "ProdutosServicos", localField: "ProdutoServicoReferencia", foreignField: "_id", as: "ProdutoServico" } }, { $lookup: { from: "Precos", localField: "PrecoReferencia", foreignField: "_id", as: "Preco" } },{$limit: 100} ]).pretty()
